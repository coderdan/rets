<?php

namespace Drupal\rets;

use Drupal\rets\Entity\RetsQuery;
use Drupal\rets\Entity\RetsServer;
use PHRETS\Models\Search\Record;
use PHRETS\Session;

/**
 * Interface QueryFetcherInterface.
 */
interface QueryFetcherInterface {

  /**
   * Runs a query to the RETS server.
   *
   * This is a wrapper for PHRETS Search.
   *
   * @param \Drupal\rets\Entity\RetsServer $retsServer
   *   The RETS server entity.
   * @param \Drupal\rets\Entity\RetsQuery $retsQuery
   *   The RETS query entity.
   * 
   * @return \PHRETS\Models\Search\Results
   *   The results from the query.
   */
  public function runQuery(RetsServer $retsServer, RetsQuery $retsQuery);

  /**
   * Runs a query to the RETS server to get media objects.
   *
   * This is a wrapper for PHRETS getObject.
   *
   * @param \Drupal\rets\Entity\RetsServer $retsServer
   *   The RETS server entity.
   * @param \Drupal\rets\Entity\RetsQuery $retsQuery
   *   The RETS query entity.
   * @param string $content_id
   *   A PHRETS result record id. ie: a ListingID or MLS id.
   *
   * @return \PHRETS\Models\Search\Results
   *   The results from the query.
   */
  public function runMediaQuery(RetsServer $retsServer, RetsQuery $retsQuery, string $content_id);

  /**
   * Get table metadata for a class.
   *
   * @param \Drupal\rets\Entity\RetsServer $retsServer
   *   A RETS server entity
   * @param \Drupal\rets\Entity\RetsQuery $retsQuery
   *   A RETS query entity.
   *
   * @return array
   *   An array of results.
   *
   * @throws \PHRETS\Exceptions\CapabilityUnavailable
   */
  public function getTableMetadata(RetsServer $retsServer, RetsQuery $retsQuery);

  /**
   * Get metadata for a class.
   *
   * This is a wrapper for PHRETS' GetClassesMetadata().
   *
   * @param \Drupal\rets\Entity\RetsServer $retsServer
   *   A RETS server entity
   * @param \Drupal\rets\Entity\RetsQuery $retsQuery
   *   A RETS query entity.
   *
   * @return array
   *   An array of results.
   */
  public function getClassesMetadata(RetsServer $retsServer, RetsQuery $retsQuery);

  /**
   * Logs into the RETS server.
   *
   * This method must be called before making any queries.
   */
  public function login(RetsServer $retsServer);

  /**
   * Logs out of the current RETS session.
   *
   * @return bool
   */
  public function disconnect();

  /**
   * Processes the query, making any adjustments needed before passing to RETS.
   *
   * @param RetsQuery $query
   *   The RETS query entity.
   */
  public function processQuery($query);

  /**
   * Process relative dates from a RETS query.
   *
   * Parses data in brackets and replaces the data with a formatted date. Uses
   * strtotime() to determine date values.
   *
   * eg: DateTimeModified={TODAY-30 days}-{NOW} would be transformed into
   * a date such as DateTimeModified=2010-07-20T19:01:00-2010-07-21T19:01:00
   *
   * @param string $query
   */
  public function parseQueryDates(&$query);

}
