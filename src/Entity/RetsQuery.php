<?php

namespace Drupal\rets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the RETS Query entity.
 *
 * @ConfigEntityType(
 *   id = "rets_query",
 *   label = @Translation("RETS Query"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\rets\RetsQueryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\rets\Form\RetsQueryForm",
 *       "edit" = "Drupal\rets\Form\RetsQueryForm",
 *       "delete" = "Drupal\rets\Form\RetsQueryDeleteForm",
 *       "test" = "Drupal\rets\Form\RetsQueryTestForm",
 *       "data" = "Drupal\rets\Form\RetsQueryDataForm",
 *       "media" = "Drupal\rets\Form\RetsQueryMediaForm",
 *       "schedule" = "Drupal\rets\Form\RetsQueryScheduleForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\rets\RetsQueryHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "rets_query",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}",
 *     "add-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/add",
 *     "edit-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/edit",
 *     "delete-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/delete",
 *     "collection" = "/admin/config/rets/rets_servers/{rets_server}/queries",
 *     "test-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/test",
 *     "data-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/data",
 *     "media-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/media",
 *     "schedule-form" = "/admin/config/rets/rets_servers/{rets_server}/queries/{rets_query}/schedule"
 *   }
 * )
 */
class RetsQuery extends ConfigEntityBase implements RetsQueryInterface {

  /**
   * Allowable count values.
   *
   * @var string[]
   */
  const COUNTS = [
    0 => 'No count',
    1 => 'Include count',
    2 => 'Only count',
  ];

  /**
   * Allowable query type.
   *
   * @var string
   */
  const QUERY_TYPE = 'DQML2';

  /**
   * Allowable response formats.
   *
   * @var string[]
   */
  const RESPONSE_FORMATS = [
    'COMPACT' => 'COMPACT',
    'COMPACT-DECODED' => 'COMPACT-DECODED',
    'STANDARD-XML' => 'STANDARD-XML',
  ];

  /**
   * The RETS Query ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The RETS Query label.
   *
   * @var string
   */
  protected $label;

  /**
   * The RETS server machine name this query belongs to.
   *
   * @var string
   */
  protected $rets_server;

  /**
   * The RETS resource type.
   *
   * @var string
   */
  protected $resource_type;

  /**
   * The RETS resource class.
   *
   * @var string
   */
  protected $class;

  /**
   * The DMQL2 query.
   *
   * @var string
   */
  protected $query;

  /**
   * Used to include a count response, or no count.
   *
   * @var int
   */
  protected $count;

  /**
   * The query type.
   *
   * @var string
   */
  protected $query_type;

  /**
   * The response format to expect from the MLS server.
   *
   * @var string
   */
  protected $response_format;

  /**
   * Limits the number of results.
   *
   * @var int
   */
  protected $limit;

  /**
   * Use standard or system names;
   *
   * Standard names = TRUE;
   * System names = FALSE;
   *
   * @var bool
   */
  protected $standard_names;

  /**
   * The entity type to store query data in.
   *
   * @var string
   */
  protected $entity_type;

  /**
   * The bundle for the entity type.
   *
   * @var string
   */
  protected $entity_bundle;

  /**
   * An array of field mappings for resource data.
   *
   * @var string[]
   */
  protected $field_map;

  /**
   * The unique key identifier for a resource.
   *
   * EG: ListingID for property resource.
   *
   * @var string
   */
  protected $key_field;

  /**
   * The RETS last modified date field for a resource.
   *
   * @var string
   */
  protected $rets_last_modified;

  /**
   * The Drupal last modified date field for a resource.
   *
   * @var string
   */
  protected $drupal_last_modified;

  /**
   * The media type.
   *
   * This is a string representing the media type. eg: 'Photo'.
   *
   * @var string
   */
  protected $media_type;

  /**
   * The Drupal media image field.
   *
   * @var string
   */
  protected $media_image_field;

  /**
   * Cron settings.
   *
   * @var array
   */
  protected $cron;

  /**
   * Media settings.
   *
   * @var array
   */
  protected $media;

  /**
   * {@inheritdoc}
   */
  public function getResourceType() {
    return $this->resource_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceClass() {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * {@inheritdoc}
   */
  public function getRetsServer() {
    return $this->rets_server;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return $this->query_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseFormat() {
    return $this->response_format;
  }

  /**
   * {@inheritdoc}
   */
  public function getLimit() {
    return $this->limit;
  }

  /**
   * {@inheritdoc}
   */
  public function getStandardNames() {
    return $this->standard_names;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationEntityType() {
    return $this->entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationEntityBundle() {
    return $this->entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMap() {
    return $this->field_map;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyField() {
    return $this->key_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastModifiedField() {
    return $this->rets_last_modified;
  }

  /**
   * @return string
   */
  public function getDrupalLastModifiedField() {
    return $this->drupal_last_modified;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaImageField() {
    return $this->media_image_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getCron() {
    return $this->cron;
  }

  /**
   * {@inheritdoc}
   */
  public function getMedia() {
    return $this->media;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFromMap(string $field, string $type) {
    if ($type !== 'drupal' && $type !== 'rets') {
      return NULL;
    }
    if ($type === 'drupal') {
      $related_type = 'rets_field';
    }
    else {
      $related_type = 'drupal_field';
    }
    $type = "{$type}_field";

    foreach ($this->getFieldMap() as $map) {
      if ($map[$type] === $field) {
        return $map[$related_type];
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMap($map) {
    $this->field_map = $map;
  }

  /**
   * {@inheritdoc}
   */
  public function setRetsQuery($query) {
    $this->query = $query;
  }

  /**
   * {@inheritdoc}
   */
  public function urlRouteParameters($rel) {
    $url_route_parameters = parent::urlRouteParameters($rel);
    // The rets server id is needed as a route parameter.
    $url_route_parameters['rets_server'] = $this->rets_server;
    return $url_route_parameters;
  }

}
