<?php

namespace Drupal\rets;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rets\Entity\RetsQuery;
use Drupal\rets\Entity\RetsServer;
use PHRETS\Configuration;
use PHRETS\Exceptions\CapabilityUnavailable;
use PHRETS\Exceptions\InvalidConfiguration;
use PHRETS\Session;

/**
 * Class QueryFetcher.
 */
class QueryFetcher implements QueryFetcherInterface {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The PHRETS session.
   *
   * @var \PHRETS\Session
   */
  protected $session;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Constructs a new QueryFetcher object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter service.
   */
  public function __construct(MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, DateFormatter $dateFormatter) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * Setter for the session property.
   *
   * @param \PHRETS\Session $session
   */
  public function setSession(Session $session) {
    $this->session = $session;
  }

  /**
   * Getter for the active PHRETS session.
   */
  public function getSession() {
    return $this->session;
  }

  /**
   * {@inheritdoc}
   */
  public function runQuery(RetsServer $retsServer, RetsQuery $retsQuery) {
    if (!$session = $this->getSession()) {
      $this->login($retsServer);
      $session = $this->getSession();
    }
    // Run the search query.
    try {
      $results = $session->Search(
        $retsQuery->getResourceType(),
        $retsQuery->getResourceClass(),
        $this->processQuery($retsQuery),
        [
          'QueryType' => $retsQuery->getQueryType(),
          'Count' => $retsQuery->getCount(),
          'Format' => $retsQuery->getResponseFormat() ?? 'COMPACT-DECODED',
          'Limit' => $retsQuery->getLimit() ?? 99999999,
          'StandardNames' => $retsQuery->getStandardNames() ?? 0,
        ]
      );
      return $results;
    }
    catch (CapabilityUnavailable $exception) {
      $this->messenger->addError($exception->getMessage());
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function runMediaQuery(RetsServer $retsServer, RetsQuery $retsQuery, string $content_id) {
    if (!$session = $this->getSession()) {
      $this->login($retsServer);
      $session = $this->getSession();
    }
    $media_settings = $retsQuery->getMedia();
    try {
      $results = $session->GetObject($retsQuery->getResourceType(), 'Photo', $content_id, '*', $media_settings['location'] ?? TRUE);
      return $results;
    }
    catch(CapabilityUnavailable $exception) {
      $this->messenger->addError($exception->getMessage());
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableMetadata(RetsServer $retsServer, RetsQuery $retsQuery) {
    if (!$session = $this->getSession()) {
      $this->login($retsServer);
      $session = $this->getSession();
    }
    try {
      $results = $session->GetTableMetadata($retsQuery->getResourceType(), $retsQuery->getResourceClass());
      return $results;
    }
    catch(CapabilityUnavailable $exception) {
      $this->messenger->addError($exception->getMessage());
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getClassesMetadata(RetsServer $retsServer, RetsQuery $retsQuery) {
    if (!$session = $this->getSession()) {
      $this->login($retsServer);
      $session = $this->getSession();
    }
    try {
      $results = $session->GetClassesMetadata($retsQuery->getResourceType());
      return $results;
    }
    catch(CapabilityUnavailable $exception) {
      $this->messenger->addError($exception->getMessage());
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function login(RetsServer $retsServer) {
    try {
      $config = Configuration::load([
        'login_url' => $retsServer->getLoginUrl(),
        'username' => $retsServer->getUsername(),
        'password' => $retsServer->getPassword(),
        'user_agent' => $retsServer->getUserAgent(),
        'user_agent_password' => $retsServer->getUserAgentPassword(),
        'rets_version' => $retsServer->getRetsVersion(),
        'http_authentication' => $retsServer->getHttpAuthentication(),
      ]);
      $session = new Session($config);
      $session->login();
      $this->setSession($session);
    }
    catch (InvalidConfiguration $exception) {
      $this->messenger->addError($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function disconnect() {
    try {
      return $this->session->Disconnect();
    }
    catch (CapabilityUnavailable $exception) {
      $this->messenger->addError($exception->getMessage());
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function processQuery($query) {
    $rets_query_string = $query->getQuery();
    $this->parseQueryDates($rets_query_string);
    return $rets_query_string;
  }

  /**
   * {@inheritdoc}
   */
  public function parseQueryDates(&$query) {
    // TODO: RETS 1.5 requires dates be converted to GMT before being sent
    // to the RETS server.
    $query = preg_replace_callback("/{(.*?)}/", function($matches){
      $time = strtotime($matches['1']);
      return $this->dateFormatter->format($time,'custom', 'c');
    },$query);
  }

}
