<?php

namespace Drupal\rets\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\rets\QueryImportManagerService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RetsImportQueueBase
 *
 * @package Drupal\Plugin\QueueWorker
 */
abstract class RetsImportQueueBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The RETS query import manager service.
   *
   * @var \Drupal\rets\QueryImportManagerService
   */
  protected $importManager;

  /**
   * RetsImportQueueBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory service.
   * @param \Drupal\rets\QueryImportManagerService $importManager
   *   The RETS query import manager service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger, QueryImportManagerService $importManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->importManager = $importManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('rests.query.import_manager')
    );
  }



}
