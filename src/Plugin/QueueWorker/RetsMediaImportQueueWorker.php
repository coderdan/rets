<?php

namespace Drupal\rets\Plugin\QueueWorker;

/**
 * A RETS content import queue worker.
 *
 * @QueueWorker(
 *   id = "rets_media_import",
 *   title = @Translation("Queue worker for RETS media"),
 *   cron = {"time" = 1600}
 * )
 *
 * @package Drupal\rets\Plugin\QueueWorker
 */
class RetsMediaImportQueueWorker extends RetsImportQueueBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    $this->importManager->createMediaContent($item);
  }

}
