<?php

namespace Drupal\rets\Plugin\QueueWorker;

use Drupal\Core\Queue\SuspendQueueException;

/**
 * A RETS content import queue worker.
 *
 * @QueueWorker(
 *   id = "rets_content_import",
 *   title = @Translation("Queue worker for"),
 *   cron = {"time" = 300}
 * )
 *
 * @package Drupal\rets\Plugin\QueueWorker
 */
class RetsContentImportQueueWorker extends RetsImportQueueBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // TODO: logger stuff.
    $entity = $this->importManager->createContent($item);
    if ($entity !== NULL) {
      /** @var \Drupal\Core\Logger\LoggerChannel $logger */
//      $logger = $this->logger->get('rets');
//
//      $logger->debug('A new entity was created from RETS content', [$entity->id()]);
    }
    else {
      throw new SuspendQueueException('A problem occurred creating a new entity from RETS content.');
    }
  }

}
