<?php

namespace Drupal\rets\Plugin\QueueWorker;

/**
 * A RETS import queue worker.
 *
 * @QueueWorker(
 *   id = "rets_queue_worker",
 *   title = @Translation("Queue worker for"),
 *   cron = {"time" = 10}
 * )
 *
 * @package Drupal\rets\Plugin\QueueWorker
 */
class RetsImportQueueWorker extends RetsImportQueueBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {
    // TODO: logger stuff.
    $this->importManager->createContent($item);
  }

}
