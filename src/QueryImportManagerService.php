<?php

namespace Drupal\rets;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rets\Entity\RetsQuery;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Class QueryImportManagerService.
 */
class QueryImportManagerService {

  /**
   * The RETS query fetcher service.
   *
   * @var \Drupal\rets\QueryFetcherInterface
   */
  protected $retsQueryFetcher;

  /**
   * The Drupal entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The Drupal entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * Constructor for the QueryImportManagerService service.
   *
   * @param \Drupal\rets\QueryFetcher $retsQueryFetcher
   *   The RETS query fetcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The Drupal queue factory service.
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Drupal entity field manager.
   */
  public function __construct(QueryFetcher $retsQueryFetcher, EntityTypeManagerInterface $entity_type_manager, QueueFactory $queueFactory, EntityFieldManager $fieldManager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->queueFactory = $queueFactory;
    $this->retsQueryFetcher = $retsQueryFetcher;
    $this->fieldManager = $fieldManager;
  }

  /**
   * Populate the RETS query worker with items.
   *
   * To populate the RETS query worker queue, we need to fetch the data from
   * RETS and add an item to the queue for each result returned.
   *
   * @param \Drupal\rets\Entity\RetsQuery $query
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function populateContentImportQueue($query) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->entityTypeManager->getStorage('rets_server')->load($query->getRetsServer());

    // Get the queue.
    $queue = $this->queueFactory->get('rets_content_import');

    // Get the query results from RETS.
    $results = $this->retsQueryFetcher->runQuery($server, $query);

    foreach ($results as $result) {
      // First, determine if we have an existing entity. If not, there is no
      // need to compare the last updated date.
      $entity = $this->contentExists($query, $result);
      if ($entity) {
        // Determine if we need to update the resource.
        if ($this->contentNeedsUpdate($query->getLastModifiedField(), $query->getDrupalLastModifiedField(), $entity, $result)) {
          // Content exists and needs an update.
          $item = new \stdClass();
          $item->entity = $entity;
          $item->data = $result;
          $item->query = $query;
          $queue->createItem($item);
        }
      }
      else {
        // Content doesn't yet exist so queue it up.
        $item = new \stdClass();
        $item->data = $result;
        $item->query = $query;
        // Inform the queue worker that the item is new setting the entity to
        // FALSE.
        $item->entity = FALSE;
        $queue->createItem($item);
      }
    }
    // Log off the RETS server.
    $this->retsQueryFetcher->disconnect();
  }

  /**
   * Populate the queue that deletes content not in the query results.
   *
   * @param RetsQuery $query
   *   The RETS query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function populateDeleteContentQueue($query) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->entityTypeManager->getStorage('rets_server')->load($query->getRetsServer());

    // Get the query results from RETS.
    $results = $this->retsQueryFetcher->runQuery($server, $query);

    // Get the queue.
    $queue = $this->queueFactory->get('rets_content_delete');
    // All content items that are not part of the result set are removed.
    $entity_type = $query->getDestinationEntityType();
    $definition = $this->entityTypeManager->getDefinition($query->getDestinationEntityType());
    if (!$bundle_key = $definition->getKey('bundle')) {
      return;
    }
    $entity_ids = $this->entityTypeManager->getStorage($entity_type)
      ->getQuery()
      ->condition($bundle_key, $query->getDestinationEntityBundle(), '=')
      ->execute();
    $result_ids = [];
    foreach ($results as $result) {
      if ($entity = $this->contentExists($query, $result)) {
        $result_ids[$entity->id()] = $entity->id();
      }
    }
    $delete_nids = array_diff($entity_ids, $result_ids);
    $entities_to_delete = $this->entityTypeManager->getStorage($entity_type)->loadMultiple($delete_nids);
    array_map(function($entity) use ($queue) {
      $item = new \stdClass();
      $item->entity = $entity;
      $queue->createItem($item);
    }, $entities_to_delete);
    // Log off the RETS server.
    $this->retsQueryFetcher->disconnect();
  }

  /**
   * Populates the queue that creates media from query results.
   *
   * @param RetsQuery $query
   *   The RETS query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function populateCreateMediaQueue($query) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->entityTypeManager->getStorage('rets_server')->load($query->getRetsServer());

    // Get the query results from RETS.
    $results = $this->retsQueryFetcher->runQuery($server, $query);

    // Items get added to the queue if the following criteria are met:
    // 1) Media fields are filled in the query media tab. AND
    // 2) The number of media items on the entity is less than from RETS OR
    // 3) The media last modified field is newer.
    $media_settings = $query->getMedia();
    if (!isset($media_settings['rets_last_modified'])
      && !isset($media_settings['drupal_last_modified'])
      && (!isset($media_settings['image_field']) || !isset($media_settings['url_field']))) {
      return;
    }
    // Get the queue.
    $queue = $this->queueFactory->get('rets_media_import');

    /** @var \PHRETS\Models\Search\Record $result */
    foreach ($results as $result) {
      if (!$entity = $this->contentExists($query, $result)) {
        continue;
      }
      /** @var \Drupal\Core\Field\FieldItemList $existing_count */
      $existing_count = $entity->get('field_media_urls')->count();
      $rets_count = $result->get($media_settings['count_field']);
      if ($existing_count < $rets_count) {
        $item = new \stdClass();
        $item->entity = $entity;
        $item->data = $result;
        $item->query = $query;
        $queue->createItem($item);
      }
      elseif ($this->contentNeedsUpdate($media_settings['rets_last_modified'], $media_settings['drupal_last_modified'], $entity, $result)) {
        $item = new \stdClass();
        $item->entity = $entity;
        $item->data = $result;
        $item->query = $query;
        $queue->createItem($item);
      }
    }
    // Log off the RETS server.
    $this->retsQueryFetcher->disconnect();
  }

  /**
   * Create content for the Query.
   *
   * @param \stdClass $item
   *
   * @return mixed
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function createContent(\stdClass $item) {
    /** @var \Drupal\rets\Entity\RetsQuery $query */
    $query = $item->query;
    // Get the field map from the query.
    $field_map  = $query->getFieldMap();
    // Check if we have an entity yet, if not, create one.
    if (!$item->entity) {
      // Create an entity with the bundle, populate fields from query mapping.
      // Before creating a new entity, we need to know the bundle info so that
      // we can grab the bundle key and use it in the create() method for the
      // entity type.
      $definition = $this->entityTypeManager->getDefinition($query->getDestinationEntityType());
      if (!$bundle_key = $definition->getKey('bundle')) {
        // TODO: Throw an exception.
        return FALSE;
      }
      $storage = $this->entityTypeManager->getStorage($query->getDestinationEntityType());
      $values = [$bundle_key => $query->getDestinationEntityBundle()];
      $entity = $storage->create($values);
    }
    else {
      $entity = $item->entity;
    }
    // Populate the entity with rets data.
    $this->saveRetsDataToEntity($field_map, $entity, $item->data);
    try {
      // Add title to entity.
      // TODO: deal with titles better.
      $entity->title->value = $item->data->get($query->getKeyField());
      $entity->save();
      return $entity;
    }
    catch (EntityStorageException $exception) {
      // TODO: catch exception and log.
      return FALSE;
    }
  }

  /**
   * Delete content.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return bool
   *   TRUE if we were able to delete.
   */
  public function deleteContent($entity) {
    try {
      $entity->delete();
      return TRUE;
    }
    catch (EntityStorageException $exception) {
      return FALSE;
    }
  }

  /**
   * Saves the field values from RETS to the entity.
   *
   * @param array $field_map
   *   The field map from the query.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity to save data to.
   * @param \PHRETS\Models\Search\Record $result
   *   The PHRETS record from RETS.
   */
  public function saveRetsDataToEntity($field_map, $entity, $result) {
    foreach ($field_map as $field) {
      $drupal_field = $field['drupal_field'];
      $rets_field = $field['rets_field'];
      if (method_exists($entity, 'hasField') && $entity->hasField($drupal_field)) {
        $entity->set($drupal_field, $result->get($rets_field));
      }
    }
  }

  /**
   * Creates media content for a query.
   *
   * @param \stdClass $item
   *   Media import object item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createMediaContent($item) {
    $entity = $item->entity;

    /** @var \Drupal\rets\Entity\RetsQueryInterface $query */
    $query = $item->query;
    $server = $this->entityTypeManager->getStorage('rets_server')->load($query->getRetsServer());

    /** @var \PHRETS\Models\Search\Record $result */
    $result = $item->data;

    $content_id = $result->get($query->getKeyField());

    $media_settings = $query->getMedia();
    $photos = [];

    // Get all photo media from rets server.
    $media = $this->retsQueryFetcher->runMediaQuery($server, $query, $content_id);

    if ($media_settings['location']) {
      foreach ($media as $data) {
        $photos[] = $data->getLocation();
      }
      $entity->set($media_settings['url_field'], $photos);
    }
    else {
      $images = [];
      $field_name = $media_settings['image_field'];
      // Get field definition.
      $field_definition = $this->getFieldDefinition($query->getDestinationEntityType(), $query->getDestinationEntityBundle(), $field_name);
      $field_settings = $field_definition->getSettings();
      $file_directory = $field_settings['file_directory'];

      $entity->set($field_name, []);
      /** @var \PHRETS\Models\BaseObject $media_item */
      foreach ($media as $id => $media_item) {
        $uri = "public://{$file_directory}/{$content_id}-{$id}.jpg";
        $file = file_save_data($media_item->getContent(), $uri, FILE_EXISTS_REPLACE);
        if ($file) {
          $images[] = [
            'target_id' => $file->id(),
            'alt' => "Listing image {$content_id}-{$id}",
            'title' => '',
          ];
        }
      }
      $entity->set($field_name, $images);
    }

    $entity->save();
  }

  /**
   * Determines if content is already exists.
   *
   * We determine if a content entity is new attempting to load a content entity
   * using the RETS key_field data value.
   *
   * @param \Drupal\rets\Entity\RetsQuery $query
   *   The RETS query we are acting on.
   * @param \PHRETS\Models\Search\Record
   *   The result from RETS.
   *
   * @return \Drupal\Core\Entity\EntityInterface|mixed
   *   The Drupal entity that exists.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function contentExists(RetsQuery $query, $result) {
    // Determine if we have an entity already. We use the RETS key field and
    // its related drupal field to lookup the entity. If we have an existing
    // entity, we then use the modified date to check if it has been updated,
    // and if so, we replace all its field data with that from RETS data.
    $storage = $this->entityTypeManager->getStorage($query->getDestinationEntityType());
    $key_field = $query->getKeyField();
    $key_value = $result->get($key_field);
    // Get the related drupal field from the field mapping.
    $drupal_key_field = $query->getFieldFromMap($key_field, 'rets');
    if ($drupal_key_field && $entity = $storage->loadByProperties([$drupal_key_field => $key_value])) {
      return reset($entity);
    }
    return FALSE;
  }

  /**
   * Checks is the given content needs an update.
   *
   * We compare the last modified date of the RETS resource to the entity's
   * last modified field.
   *
   * @param string $rets_last_modified
   *   The RETS last modified date field name.
   * @param string $drupal_last_modified
   *   The Drupal last modified date field name.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Drupal entity we are comparing RETS to.
   * @param \PHRETS\Models\Search\Record $result
   *   The RETS resource result.
   *
   * @return bool
   *   TRUE if the content needs an update.
   */
  public function contentNeedsUpdate($rets_last_modified, $drupal_last_modified, $entity, $result) {
    if (method_exists($entity, 'hasField') && $entity->hasField($drupal_last_modified)) {
      if (!$drupal_date = $entity->$drupal_last_modified->value) {
        // No value stored for this Drupal last modified field, so the content
        // needs an update.
        return TRUE;
      }
    }

    $rets_date = $result->get($rets_last_modified);
    $drupal_datetime_value = new DrupalDateTime($drupal_date);
    $rets_datetime_value = new DrupalDateTime($rets_date);
    if ($drupal_datetime_value < $rets_datetime_value) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get a field definition for a field.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle
   *   The entity bundle.
   * @param string $field_name
   *   The field name to get the definition for.
   *
   * @return \Drupal\field\Entity\FieldConfig
   *   The field definition.
   */
  public function getFieldDefinition($entity_type_id, $bundle, $field_name) {
    $definitions = $this->fieldManager->getFieldDefinitions($entity_type_id, $bundle);
    return $definitions[$field_name];
  }

}
