<?php

namespace Drupal\rets\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\rets\QueryFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Query test form.
 *
 * Performs a test query against the RETS server using the query configuration.
 *
 * @package Drupal\rets\Form
 */
class RetsQueryTestForm extends RetsQueryFormBase {

  /**
   * The RETS query fetcher service.
   *
   * @var \Drupal\rets\QueryFetcher
   */
  protected $fetcher;

  /**
   * RetsQueryTestForm constructor.
   *
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger service.
   * @param \Drupal\rets\QueryFetcher $fetcher
   *   The RETS query fetcher service.
   */
  public function __construct(Messenger $messenger, QueryFetcher $fetcher) {
    $this->setMessenger($messenger);
    $this->fetcher = $fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('rets.query_fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // This container will be replaced by AJAX.
    $form['container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Test query'),
      '#attributes' => ['id' => 'test-query-container'],
    ];
    $form['container']['test_results'] = [
      '#type' => 'markup',
      '#markup' => '<p>Click the Test button to run the query.</p>',
    ];

    // Change the submit button text.
    $actions['submit']['#value'] = $this->t('Test');

    // Add test photos ajax enabled button.
    $form['container']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Test Query'),
      '#submit' => [
        '::submitForm',
        '::save',
      ],
      '#ajax' => [
        'callback' => '::testQuery',
        'wrapper' => 'test-query-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Fetching data from the RETS server...'),
        ],
      ],
    ];

    /** @var \Drupal\rets\Entity\RetsQuery $query */
    $query = $this->getRouteMatch()->getParameter('rets_query');

    // This container will be replaced by AJAX.
    $form['photo-container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Test media'),
      '#attributes' => ['id' => 'test-photo-query-container'],
    ];
    $form['photo-container']['photo'] = [
      '#type' => 'html_tag',
      '#tag' => 'img',
      '#value' => '',
    ];
    $form['photo-container']['key_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Resource ID'),
      '#description' => $this->t('Enter a value of a valid resource id. This should be a @key_field value for a Property resource.', ['@key_field' => $query->getKeyField()])
    ];
    $form['photo-container']['test_photo_results'] = [
      '#type' => 'markup',
      '#markup' => '<p>Click the Test Photo button to test getting photo objects.</p>',
    ];

    $form['photo-container']['photo-submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Test Photos'),
      '#submit' => [
        '::submitForm',
        '::save',
      ],
      '#ajax' => [
        'callback' => '::testPhotoQuery',
        'wrapper' => 'test-photo-query-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Fetching data from the RETS server...'),
        ],
      ],
    ];

    $form['test-dqml-container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Test DMQL'),
      '#attributes' => ['id' => 'test-dmql-query-container'],
    ];

    $form['test-dqml-container']['dmql'] = [
      '#type' => 'textarea',
      '#title' => 'DMQL2 query',
      '#rows' => 5,
      '#default_value' => $form_state->getValue('dmql') ?? '(PropertyCategory=|Res),(ListingID=19235606)',
    ];

    $form['test-dqml-container']['output'] = [
      '#type' => 'markup',
      '#markup' => '',
    ];

    $form['test-dqml-container']['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Test DMQL'),
      '#submit' => [
        '::submitForm',
        '::save',
      ],
      '#name' => 'dmql-test-submit',
      '#ajax' => [
        'callback' => '::testDmql',
        'wrapper' => 'test-dmql-query-container',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Fetching data from the RETS server...'),
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    // Get the basic actions from the base class.
    $actions = parent::actions($form, $form_state);

    // Remove the submit button.
    unset($actions['submit']);

    // Remove delete option, since this form isn't controlling an entity.
    unset($actions['delete']);

    // Return the result.
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check if the photo submit button was pressed. If so, validate there
    // is a key_field value, if not, throw a form error.
    $trigger = $form_state->getTriggeringElement();
    if (!empty($trigger['#ajax']['wrapper']) && $trigger['#ajax']['wrapper'] === 'test-photo-query-container') {
      // We have our man.
      $key_field = $form_state->getValue('key_field');
      if (!$key_field) {
        $form_state->setErrorByName('key_field', $this->t('Enter a resource id.'));
        return FALSE;
      }
    }
  }

  /**
   * Tests that the query is successful.
   *
   * We return just the count of results rather than printing all of the data.
   * This is just a test, after all.
   */
  public function testQuery(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->getRouteMatch()->getParameter('rets_server');
    $query = $this->getRouteMatch()->getParameter('rets_query');
    try {
      $results = $this->fetcher->runQuery($server, $query);
      $element = $form['container'];
      $count = $results->getTotalResultsCount();
      $resource = $results->getResource();
      $element['test_results']['#markup'] = "<div>{$count} results returned for the {$resource} resource.</div>";
      $this->messenger->addMessage($this->t('Successfully queried the %server server', ['%server' => $server->label()]));
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
      return $form['status_messages'];
    }
    return $element;
  }

  /**
   * Tests that getting media is successful.
   */
  public function testPhotoQuery(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->getRouteMatch()->getParameter('rets_server');
    /** @var \Drupal\rets\Entity\RetsQuery $query */
    $query = $this->getRouteMatch()->getParameter('rets_query');

    $media_settings = $query->getMedia();
    if (!$form_state->getValue('key_field')) {
      return $form['photo-container'];
    }
    else {
      try {
        $result = $this->fetcher->runMediaQuery($server, $query, $form_state->getValue('key_field'), $media_settings['location'] ?? TRUE);
        $location = $result->first()->getLocation();
        if ($location) {
          $element = $form['photo-container'];
          $element['photo']['#attributes']['src'] = $location;
          $element['photo']['#attributes']['width'] = '250px';
          $element['photo']['#attributes']['height'] = 'auto';
          $this->messenger->addMessage($this->t('Photo retrieved successfully from the %server server', ['%server' => $server->label()]));
        }
        else {
          // Maybe there was no media object.
          $this->messenger->addWarning($result->first()->GetContent());
          return $form['photo-container'];
        }
      }
      catch (\Exception $e) {
        $this->messenger->addError($e->getMessage());
        return $form['status_messages'];
      }
    }
    return $element;
  }

  public function testDmql(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\rets\Entity\RetsServer $server */
    $server = $this->getRouteMatch()->getParameter('rets_server');
    /** @var \Drupal\rets\Entity\RetsQuery $query */
    $query = $this->getRouteMatch()->getParameter('rets_query');
    $query->setRetsQuery($form_state->getValue('dmql'));
    $results = $this->fetcher->runQuery($server, $query);
    $element = $form['test-dqml-container'];
    $element['output']['#markup'] = '<pre>' . print_r($results, true) . '</pre>';
    return $element;
  }

}
