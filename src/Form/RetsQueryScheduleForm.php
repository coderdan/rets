<?php

namespace Drupal\rets\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RetsQueryMediaForm.
 */
class RetsQueryScheduleForm extends RetsQueryFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * RetsQueryScheduleForm constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The Drupal module handler service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   */
  public function __construct(ModuleHandler $moduleHandler, ConfigFactory $configFactory, DateFormatter $dateFormatter) {
    $this->moduleHandler = $moduleHandler;
    $this->configFactory = $configFactory;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\rets\Entity\RetsQueryInterface $rets_query */
    $rets_query = $this->entity;

    $form['#tree'] = TRUE;

    $options = [1800, 3600, 10800, 21600, 43200, 86400];

    $form['cron'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Cron settings'),
    ];

    $form['cron']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import on Cron'),
      '#description' => $this->t('Check this box to enable content import on cron'),
      '#default_value' => $rets_query->getCron()['enabled'] ?? '',
    ];

    $form['cron']['interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Resource Import Interval'),
      '#description' => $this->t('Import interval for content resources. Can only occur as often as cron runs.'),
      '#default_value' => $rets_query->getCron()['interval'] ?? '',
      '#options' => [0 => t('- Select -')] + array_map([$this->dateFormatter, 'formatInterval'], array_combine($options, $options)),
      '#states' => [
        'visible' => [
          ':input[name="cron[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['cron']['delete_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete on Cron'),
      '#description' => $this->t('Check this box to enable content deletions on cron'),
      '#default_value' => $rets_query->getCron()['delete_enabled'] ?? '',
    ];

    $form['cron']['delete_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Resource Delete Interval'),
      '#description' => $this->t('Delete interval for content resources. Can only occur as often as cron runs.'),
      '#default_value' => $rets_query->getCron()['delete_interval'] ?? '',
      '#options' => [0 => t('- Select -')] + array_map([$this->dateFormatter, 'formatInterval'], array_combine($options, $options)),
      '#states' => [
        'visible' => [
          ':input[name="cron[delete_enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['cron']['media_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Import media on Cron'),
      '#description' => $this->t('Check this box to enable media import on cron'),
      '#default_value' => $rets_query->getCron()['media_enabled'] ?? '',
    ];

    $form['cron']['media_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Media Import Interval'),
      '#description' => $this->t('Import interval for media resources. Can only occur as often as cron runs.'),
      '#default_value' => $rets_query->getCron()['interval'] ?? '',
      '#options' => [0 => t('- Select -')] + array_map([$this->dateFormatter, 'formatInterval'], array_combine($options, $options)),
      '#states' => [
        'visible' => [
          ':input[name="cron[media_enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rets_query = $this->entity;
    $status = $rets_query->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Updated the %label RETS Query schedule settings.', [
          '%label' => $rets_query->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label RETS Query schedule settings.', [
          '%label' => $rets_query->label(),
        ]));
    }
    $form_state->setRedirectUrl($rets_query->toUrl('collection'));
  }

}
