<?php

namespace Drupal\rets\Form;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RetsQueryMediaForm.
 */
class RetsQueryMediaForm extends RetsQueryFormBase {

  /**
   * The Drupal entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * RetsQueryDataForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Drupal entity field manager.
   */
  public function __construct(EntityFieldManager $fieldManager) {
    $this->fieldManager = $fieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\rets\Entity\RetsQueryInterface $rets_query */
    $rets_query = $this->entity;

    $media_settings = $rets_query->getMedia();

    $form['#tree'] = TRUE;

    $form['media']['rets_last_modified'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RETS Last modified field'),
      '#description' => $this->t('The RETS last modified field name. This is a date field that is updated when media is changed by an agent or the MLS.'),
      '#default_value' => $media_settings['rets_last_modified'] ?? '',
    ];

    $form['media']['drupal_last_modified'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal last modified field'),
      '#description' => $this->t('The Drupal field to store the media last modified date from RETS. This is used to check if the media needs to be updated.'),
      '#default_value' => $media_settings['drupal_last_modified'] ?? '',
    ];

    $form['media']['count_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RETS photo count field name'),
      '#description' => $this->t('The RETS field that tracks the number of photos.'),
      '#default_value' => $media_settings['count_field'] ?? '',
    ];

    $form['media']['location'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store Media as URL'),
      '#description' => $this->t('Check the box to store URLs rather than as a binary file.'),
      '#default_value' => $media_settings['location'] ?? '',
    ];

    $form['media']['image_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal image field'),
      '#description' => $this->t('The Drupal image field responsible for storing the photo media from RETS. This must be an image file field.'),
      '#default_value' => $media_settings['image_field'] ?? '',
      '#states' => [
        'invisible' => [
          ':input[name="media[location]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['media']['url_field'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal media url field'),
      '#description' => $this->t('The Drupal image field responsible for storing the photo media URL from RETS. This must be a text field.'),
      '#default_value' => $media_settings['url_field'] ?? '',
      '#states' => [
        'invisible' => [
          ':input[name="media[location]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['media']['type'] = [
      '#type' => 'value',
      '#value' => 'Photo',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rets_query = $this->entity;
    $status = $rets_query->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Updated the %label RETS Query media settings.', [
          '%label' => $rets_query->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label RETS Query media settings.', [
          '%label' => $rets_query->label(),
        ]));
    }
    $form_state->setRedirectUrl($rets_query->toUrl('collection'));
  }

}
